# Git
## 密钥生成
	ssh-keygen -t rsa -C "YOUR_EMAIL@YOUREMAIL.COM"
## git常用命令
	1.取回远程主机某个分支的更新，再与本地的指定分支合并
		git pull <远程主机名> <远程分支名>:<本地分支名>
	2.在目录中创建新的 Git 仓库
		git init 
	3.添加服务端链接
		git remote add [name] [url]
	4.将文件加入快照（缓存区）
		git add [path]
	5.提交缓存区文件到仓库
		git commit -m [comments]
			-m 注释
			-a 跳过 add
	6.上传到服务端
		git push -u [remote] [branch]
	7.同步项目到本地
	    git clone [url]

    8.状态查看
	git status
		-s 显示简略信息
	git diff
		显示版本不同内容
		
	9.跳过ssl认证
	
	git -c http.sslVerify=false clone
		
		

## 新项目初始化

### 1.Git global setup

    git config --global user.name "username"
    git config --global user.email "email-addr"

### 2.初始化项目

#### case1：create new

    1.复制项目到本地
        git clone [project url]

    2.上传新文件
        touch README.md
        git add README.md
        git commit -m "add README"
        git push -u origin master

#### case2:Existing folder

    cd existing_folder
    git init
    git remote add origin [project url]

    git add .
    git commit -m "Initial commit"
    git push -u origin master

#### case3:Existing Git repository

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin  [project url]
    git push -u origin --all
    git push -u origin --tags